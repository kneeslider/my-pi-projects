# benoetigte module
import time
import os
import urllib
import sys

ablagepfad = "/home/pi/ISGWeb"

#Pfad zusammensetzen und existens pruefen
pfad = ablagepfad
if not os.path.exists(pfad):
    os.makedirs(pfad)

pfad = pfad + "/" + time.strftime("%Y")
if not os.path.exists(pfad):
    os.makedirs(pfad)

pfad = pfad + "/" + time.strftime("%m")
if not os.path.exists(pfad):
    os.makedirs(pfad)

pfad = pfad + "/" + time.strftime("%d")
if not os.path.exists(pfad):
    os.makedirs(pfad)

pfad = pfad + time.strftime("/isg_%Y-%m-%d_%H-%M-%S.txt")


# ISG Web Ist/Sollwerte auslesen
# Fehler werden abgefangen
try:
    f = urllib.urlopen("http://192.168.0.20/?s=1,0")
    s = f.read()
except IOError, e:
    Fehlertext = str(e)
    if os.system("ping -c 1 " + "192.168.0.1") != 0:
        Fehlertext =  " Router ist NICHT erreichbar :" + Fehlertext
    elif os.system("ping -c 1 " + "192.168.0.20") != 0:
        Fehlertext =  " ISGWeb ist NICHT erreichbar :" + Fehlertext
    else:
        Fehlertext =  " Unbekannter Fehler bei urllig.urlopen :" + Fehlertext
    fobj_out = open(ablagepfad + "/isgweb_log_logdatei.txt","a")
    fobj_out.write(time.strftime("%d.%m.%Y %H:%M:%S") + Fehlertext + "\n")
    fobj_out.close()
    sys.exit()

#Ausgabe der ISGWeb Html Seite in eine Datei
fobj_out = open(pfad,"w")
fobj_out.write(s)
fobj_out.close()

# Bestaetigung ausgeben
print(time.strftime("%d.%m.%Y %H:%M:%S"), "ISGWeb geschrieben nach: " + pfad)

#Eintrag in die Logdatei
fobj_out = open(ablagepfad + "/isgweb_log_logdatei.txt","a")
fobj_out.write(time.strftime("%d.%m.%Y %H:%M:%S") + " ISGWeb geschrieben nach: " + pfad + "\n")
fobj_out.close()