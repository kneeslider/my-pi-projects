#!/bin/bash
#
# Titel: FTP Transfer Script
# Description: Datei automatisch auf einen festgelegten FTP-Server hochladen.
# Version: 0.1

### EINSTELLUNGEN

FTP_SERVER="192.168.0.30"
FTP_USER="isg"
FTP_PASS="isg"

#Die Datei welche uebertragen werden soll
FILE2TRANSFER="/LOG/Tage/2014/ISG-2014-12-26.csv"

#Das Verzeichnis wohin die Datei uebertragen werden soll
REMOTEDIR="/home/ISGWeb/LOG/Tage/2014/"

### ENDE DER EINSTELLUNGEN


# Dateien per FTP auf den Server schieben
ftp -ni << END_UPLOAD
open $FTP_SERVER
user $FTP_USER $FTP_PASS
cd $REMOTEDIR
bin
mget $FILE2TRANSFER
quit
END_UPLOAD

exit 0
