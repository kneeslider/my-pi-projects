import shutil
import os

def copyFile(src, dest):
    try:
        shutil.rmtree(dest)
        shutil.copytree(src, dest)
        os.chmod(dest,0777)
    # eg. src and dest are the same file
    except shutil.Error as e:
        print('Error: %s' % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        print('Error: %s' % e.strerror)

copyFile("/usr/share/adafruit/webide/repositories/my-pi-projects/ISG_v0", "/home/pi/ISGWeb/Code")

Inhalt = os.listdir("/home/pi/ISGWeb/Code")
for i in Inhalt:
    os.chmod("/home/pi/ISGWeb/Code" + "/" + i, 0777)

print "Kopieren ausgefuehrt"