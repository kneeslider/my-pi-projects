#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-


import sgmllib
import os
import codecs
import time

pfad = "/home/pi/ISGWeb"
class ISG_parse_messwerte(sgmllib.SGMLParser):
    "A simple parser class."

    def parse(self, s):
        "Parse the given string 's'."
        self.feed(s)
        self.close()

    def __init__(self, verbose=0):
        "Initialise an object, passing 'verbose' to the superclass."

        sgmllib.SGMLParser.__init__(self, verbose)

        self.kanalname = []
        self.kanalwert = []
        

        self.tabkanalname = 0
        self.tabkanalwert = 0
        self.binaer = -1

    def start_td(self, attributes):
        #Wenn der HTML mit "<td" beginnt
        for name, value in attributes:
            if name == "class":
                #Alle Zeilen mit : "<td class=" werden beruecksichtigt
                if value =="key" or value == "key round-leftbottom":
                    self.tabkanalname = 1
                if value == "value" or value == "value round-rightbottom":
                    self.tabkanalwert = 1
 
    def end_td(self):
        self.tabkanalname =0
        self.tabkanalwert =0
        self.binaer = -1
    
    def start_img(self, attributes):
        #Wenn der HTML mit "<img" beginnt
        if self.tabkanalwert == 1:
            for name, value in attributes:
                if name == "src":
                    if value =="./pics/ste-symbol_an-97b765.png":
                        self.binaer = 1
                    else:
                        self.binaer = 0
    
    def handle_data(self, data):
        
        if self.tabkanalname ==1:
            self.kanalname.append(data)
        if self.tabkanalwert ==1:
            if data ==" " or data =="":
                if self.binaer == 1:
                    self.kanalwert.append(1)
            else:
                # Zahlenwert von Einheit trennen
                ergebnis = ""
                for i in data:
                    if i.isdigit() or i ==",":
                        ergebnis = ergebnis + i
                    else:
                        break
                self.kanalwert.append(ergebnis)    
                

    def get_kanalname(self):
        "Return the list of kanalname."
        return self.kanalname

    def get_kanalwert(self):
        "Return the list of kanalname."
        return self.kanalwert


import urllib
import csv

"ISGMesswerte vom ISG Gerät holen und in String 's' einlesen "
f = urllib.urlopen("http://192.168.0.20/?s=1,0")
#f = open(pfad + "/2014/06/01/isg_2014-06-01_00-03-02.txt","r")
s = f.read()
f.close()

" Aufspalten in Kanalnamen und Messwerte und übergeben in die Array Mess_Name und Mess_Wert"
" The class 'ISG_parse_messwerte' should have been defined first, remember."
myparser = ISG_parse_messwerte()
myparser.parse(s)

Mess_Name = myparser.get_kanalname()
Mess_Wert = myparser.get_kanalwert()
Mess_Check = [0] * len(Mess_Name)
Ausg_Name = "Zeit; "
Ausg_Einheit ="yyyy-mm-dd hh:mm:ss; "
Ausg_Wert = time.strftime("%Y-%m-%d %H:%M:%S") + "; "
#print Mess_Name
#print Mess_Wert
#print Mess_Check
"Mess_Name der Labelkonfig zuordnen und bestimmen ob ausgegeben wird"
reader = csv.DictReader(open(pfad + "/labelconfig.csv", "rb"), delimiter=";")
for row in reader:
    if int(row["Import"]) > 0:
        Ausg_Name = Ausg_Name + str(row["Kanalname"]) + "; "
        Ausg_Einheit = Ausg_Einheit + str(row["Einheit"]) + "; "
        Kontrolle = 0
    for i in range(len(Mess_Name)):
        "Gleicher Kanalname zwischen Konfig und ausgelesenem Array"
        #if str(row["ISG_Name"]).find(Mess_Name[i])<>-1:
        if (Mess_Name[i].find(str(row["ISG_Name"]))<>-1 and str(row["ISG_Name"]).find(Mess_Name[i])<>-1):    
             #print Mess_Name[i],str(row["ISG_Name"])
            Mess_Check[i] = 1
            Kontrolle = 1
            if int(row["Import"]) > 0:
                Ausg_Wert = Ausg_Wert + str(Mess_Wert[i]) + "; "
                break
    if Kontrolle == 0:
        if int(row["Import"]) == 2:
            Ausg_Wert = Ausg_Wert + "0; "
        else:
            Ausg_Wert = Ausg_Wert + "nv; "

Ausg_Name = Ausg_Name + "Sonstiges; " 
Ausg_Einheit = Ausg_Einheit + "-; " 

for i in range(len(Mess_Name)):
    if Mess_Check[i] == 0:
        Ausg_Wert = Ausg_Wert + Mess_Name[i] + "=" + str(Mess_Wert[i]) + ","
Ausg_Wert=Ausg_Wert + ";"

Ausg_Name = Ausg_Name + "\n" 
Ausg_Einheit = Ausg_Einheit + "\n" 
Ausg_Wert = Ausg_Wert + "\n" 

#print Ausg_Name
#print Ausg_Einheit 
#print Ausg_Wert 


#Ausgabe in die Datei
#Ordner: Existenz prüfen und ggf anlegen mit Berechtigungen

if not os.path.isdir(pfad + "/LOG/Tage/" + str(time.strftime("%Y"))):
    os.makedirs(pfad + "/LOG/Tage/" + str(time.strftime("%Y")))
    os.chmod(pfad + "/LOG/Tage/" + str(time.strftime("%Y")),0777)

#Datei: Existents prüfen und ggf anlegen mit Berechtigungen und Header

if not os.path.isfile(pfad + "/LOG/Tage/" + str(time.strftime("%Y")) + "/ISG-" + time.strftime("%Y-%m-%d") + ".csv"):
    f = file(pfad + "/LOG/Tage/" + str(time.strftime("%Y")) + "/ISG-" + time.strftime("%Y-%m-%d") + ".csv", 'w')
    f.write(Ausg_Name)
    f.write(Ausg_Einheit)
    f.close
    os.chmod(pfad + "/LOG/Tage/" + str(time.strftime("%Y")) + "/ISG-" + time.strftime("%Y-%m-%d") + ".csv",0777)
else:
    f = file(pfad + "/LOG/Tage/" + str(time.strftime("%Y")) + "/ISG-" + time.strftime("%Y-%m-%d") + ".csv", 'a')
#Messwerte schreiben
f.write(Ausg_Wert)
f.close
